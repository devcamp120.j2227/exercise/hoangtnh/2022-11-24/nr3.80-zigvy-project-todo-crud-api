//khai báo thư viện mongoose
const mongoose = require("mongoose");
//khai báo schema
const Schema = mongoose.Schema;

const PhotoSchema = new Schema ({
    _id: {
        type: mongoose.Types.ObjectId
    },
    albumId: {
        type: mongoose.Types.ObjectId,
        ref: "album"
    },
    title: {
        type: String,
        required : true
    },
    url: {
        type: String,
        required: true
    },
    thumbnailUrl: {
        type: String,
        required: true
    }
})
module.exports = mongoose.model("photo", PhotoSchema)