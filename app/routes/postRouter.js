//import express js
const express = require("express");
const router = express.Router();
const postController = require("../controllers/postController");

router.post("/posts", postController.createPost);
// router.get("/posts", postController.getAllPost);
router.put("/posts/:postId", postController.updatePost);
router.get("/posts/:postId", postController.getPostById);
router.delete("/posts/:postId", postController.deletePost);
router.post("/users/:userId/posts", postController.createPostToUserId);
router.get("/users/:userId/posts", postController.getAllPostOfUser);
router.get("/posts", postController.getAllPostByQuery);
module.exports = router;