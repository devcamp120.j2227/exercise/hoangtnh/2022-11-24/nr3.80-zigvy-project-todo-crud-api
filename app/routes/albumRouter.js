const express = require("express");
const albumController = require("../controllers/albumController");
const router = express.Router();

router.post("/albums", albumController.createAlbum);
//router.get("/albums", albumController.getAllAlbum);
router.put("/albums/:albumId", albumController.updateAlbum);
router.get("/albums/:albumId", albumController.getAlbumById);
router.delete("/albums/:albumId", albumController.deleteAlbum);
router.post("/users/:userId/albums", albumController.createAlbumToUserId);
router.get("/users/:userId/albums", albumController.getAllAlbumOfUser);
router.get("/albums", albumController.getAllAlbumByQuery);
module.exports = router;