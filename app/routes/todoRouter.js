//import express Js
const express = require("express");
//Khai báo router
const router = express.Router();
//import todo controller
const todoController = require("../controllers/todoController");

router.post("/todo", todoController.createTodo);
//router.get("/todo", todoController.getAllTodo);
router.put("/todo/:todoId", todoController.updateTodo);
router.get("/todo/:todoId", todoController.getTodoById);
router.delete("/todo/:todoId", todoController.deleteTodo);
router.post("/users/:userId/todo", todoController.createTodoToUserId);
router.get("/users/:userId/todo", todoController.getAllTodoOfUser);
router.get("/todo", todoController.getAllTodoByQuery);
module.exports = router;