const mongoose = require("mongoose");
const todoModel = require("../models/todoModel");
const userModel = require("../models/userModel");

//function create to do
const createTodo = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    let body = request.body;

    //B2: validate dữ liệu
    if(!body.title ){
        return response.status(400).json({
            status:"Bad request",
            message:"Title is not valid"
        })
    } 
   
    //B3: thao tác với CSDL
    let newTodo = {
        _id: mongoose.Types.ObjectId(),
        userId : mongoose.Types.ObjectId(),
        title: body.title,
        completed: body.completed
    }
    if(body.title !== undefined){
        newTodo.title = body.title
    }
    if(body.completed !== undefined){
        newTodo.completed = body.completed
    }
    todoModel.create(newTodo, (error,data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status:"Create new to do successfully",
            data: data
        })
    })
}

//function get all photo
const getAllTodo = (request, response) => {
    todoModel.find((error,data) =>{
        if(error) {
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:" Get all to do successfully",
            data: data
        })
    })
}
//function update todo by id
const updateTodo = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    let todoId = request.params.todoId;
    let body = request.body

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(todoId)){
        return response.status(400).json({
            status:"Bad request",
            message:" To do id is not valid"
        })
    }
    if(!body.title ){
        return response.status(400).json({
            status:"Bad request",
            message:"Title is not valid"
        })
    } 
    if(!body.completed ){
        return response.status(400).json({
            status:"Bad request",
            message:"Completed is not valid"
        })
    } 
    //B3: gọi model thao tác CSDL
    const todoUpdate = {};
    if(body.title !== undefined){
        todoUpdate.title = body.title
    }
    if(body.completed !== undefined){
        todoUpdate.completed = body.completed
    }
    todoModel.findByIdAndUpdate(todoId, todoUpdate, (error, data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status:`Update to do with id${todoId} successfully`,
            data: data
        })
    })
}
//function get todo by id
const getTodoById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    let todoId = request.params.todoId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(todoId)){
        return response.status(400).json({
            status:"Bad request",
            message:"To do id is not valid"
        })
    }
    //B3: gọi model chứa id thao tác với csdl
    todoModel.findById(todoId,(error, data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:`Get Todo by id ${todoId} successfully`,
            data: data
        })
    })
}
//function delete to do by id
const deleteTodo = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    let todoId = request.params.todoId;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(todoId )){
        return response.status(400).json({
            status:"Bad request",
            message: "Photo id is not valid"
        })
    }
    //B3: thao tác với CSDL
    todoModel.findByIdAndRemove(todoId,(error, data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:`Deleted todo by id ${todoId} successfully`
        })
    })
}
//function create todo and push to user via todo obj
const createTodoToUserId = (request, response) => {
    //B1 chuẩn bị dữ liệu
    const userId = request.params.userId;
    const body = request.body;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return response.status(400).json({
            status:"Bad request",
            message: " User id is not valid"
        })
    }
    if(!body.title ){
        return response.status(400).json({
            status:"Bad request",
            message:"Title is not valid"
        })
    } 
    
    //B3: thao tác với CSDL
    let newTodo = {
        _id: mongoose.Types.ObjectId(),
        userId : userId,
        title: body.title,
        completed: body.completed
    }
   
    todoModel.create(newTodo,(error, data) =>{
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        //Thêm Id của todo mới vào mảng toDo trong user
        userModel.findByIdAndUpdate(userId, {
            $push: {
                toDo: data._id
            }
        }, (err,updateUser) =>{
            if(err){
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
            return response.status(201).json({
                status:"Create to do successfully",
                data: data
            })
        })
    })
}
//function get all todo of user
const getAllTodoOfUser = (request, response) =>{
    const userId = request.params.userId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "User ID không hợp lệ"
        })
    }

    // B3: Thao tác với cơ sở dữ liệu
    userModel.findById(userId)
        .populate("toDo")
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }

            return response.status(200).json({
                status: "Get all To do of user successfully",
                data: data.toDo
            })
        })
       
}
//function get all to do by user id using query
const getAllTodoByQuery = (request, response) =>{
    const userId = request.query.userId;
    //nếu truyền vào user Id query
    if(userId){
        if(!mongoose.Types.ObjectId.isValid(userId)) {
            return response.status(400).json({
                status: "Error 400: Bad Request",
                message: "User ID is invalid"
            })
        }
        userModel.findById(userId)
            .populate("toDo")
            .exec((error,data) =>{
            if(error){
                return response.status(500).json({
                    status:"Internal server error",
                    message: error.message
                })
            }
            return response.status(200).json({
                status:"Get To do by user Id successfully",
                data: data.toDo

            })
        })
    }
    //nếu không truyền vào user id
    else{
        todoModel.find((error,data) =>{
            if(error){
                return response.status(500).json({
                    status:"Internal server error",
                    message: error.message
                })
            }
            return response.status(200).json({
                status:"Get all to do successfully",
                data: data

            })
        })
    }
    
}
module.exports = {
    createTodo,
    //getAllTodo,
    updateTodo,
    getTodoById,
    deleteTodo,
    createTodoToUserId,
    getAllTodoOfUser,
    getAllTodoByQuery
}